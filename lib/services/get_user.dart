import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

Future<Set> getUser() async {
  final prefs = await SharedPreferences.getInstance();
  final retrieveToken =
      jsonDecode(prefs.getString("user")!) as Map<String, dynamic>?;
  final token = retrieveToken!["token"];
  final userId = retrieveToken["_id"];
  return {token, userId};
}
