import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/UI/cart/cart.dart';
import 'package:shop/UI/checkout/checkout.dart';
import 'package:shop/UI/details/detail_product.dart';
import 'package:shop/UI/favorite/favorite.dart';
import 'package:shop/UI/popular/most_popular.dart';
import 'package:shop/UI/loading/loading_screen.dart';
import 'package:shop/connexionStatus/handling_connection.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/helpers/custom_route_transition.dart';
import 'package:shop/provider/authentication_provider.dart';
import 'package:shop/provider/cart_provider.dart';
import 'package:shop/provider/checkout_provider.dart';
import 'package:shop/provider/product_provider.dart';
import 'package:shop/routes/app_route.dart';

import 'UI/authentication/authentication.dart';
import 'UI/home/home.dart';

void main() async {
  // await GetStorage.init();
  HttpOverrides.global = MyHttpOverrides();

  runApp(
    const Shop(),
  );
}

class MyHttpOverrides extends HttpOverrides {
  // @override
  HttpClient createHttpclient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) {
        final isValidHost = host == "https://shopappcm.herokuapp.com";
        return isValidHost;
      };
  }
}

class Shop extends StatelessWidget {
  const Shop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => AuthenticationProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => ProductsProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => CartProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => CheckoutProvider(),
        )
      ],
      child: Consumer<AuthenticationProvider>(
        builder: (BuildContext context, authenticated, Widget? child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              textTheme:
                  Theme.of(context).textTheme.apply(bodyColor: kTextColor),
              fontFamily: "Roboto",
              pageTransitionsTheme: PageTransitionsTheme(
                builders: {
                  TargetPlatform.android: CustomPageTransitionBuilder(),
                  TargetPlatform.iOS: CustomPageTransitionBuilder(),
                },
              ),
            ),
            home: authenticated.isAuthenticated
                ? const Home()
                : FutureBuilder(
                    initialData: authenticated.tryAutoLogin(),
                    future: authenticated.tryAutoLogin(),
                    builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? const LoadingScreen()
                            : const AuthenticationScreen(),
                  ),
            routes: {
              AppRoutes.homePage: (ctx) => const Home(),
              AppRoutes.detailPage: (ctx) => const DetailProduct(),
              AppRoutes.cartPage: (ctx) => const Cart(),
              AppRoutes.checkoutPage: (ctx) => const Checkout(),
              AppRoutes.favoritePage: (ctx) => const FavoriteScreen(),
              AppRoutes.mostPopularPage: (ctx) => const MostPopular(),
              AppRoutes.connectionErrorPage: (ctx) =>
                  const HandlingConnectionState(),
            },
          );
        },
      ),
    );
  }
}