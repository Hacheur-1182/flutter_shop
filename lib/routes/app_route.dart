class AppRoutes {
  static const homePage = "/home";
  static const detailPage = "/details";
  static const cartPage = "/cart";
  static const checkoutPage = "/checkout";
  static const favoritePage = "/favorite";
  static const mostPopularPage = "/popular";
  static const connectionErrorPage = "/connectionErrorPage";
}
