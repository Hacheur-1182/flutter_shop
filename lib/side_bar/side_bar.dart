import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/provider/authentication_provider.dart';
import 'package:shop/routes/app_route.dart';

class SideBar extends StatelessWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: const Text("Menu"),
            centerTitle: true,
            backgroundColor: Colors.black,
            elevation: 0.0,
            automaticallyImplyLeading: false,
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.favorite),
            title: const Text("Favorite"),
            onTap: () {
              Navigator.pushNamed(
                context,
                AppRoutes.favoritePage,
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.payment_outlined),
            title: const Text("Checkout"),
            onTap: () {
              Navigator.pushNamed(
                context,
                AppRoutes.checkoutPage,
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text("logout"),
            onTap: () {
              // Navigator.pop(context);
              Navigator.pushReplacementNamed(context, "/");
              Provider.of<AuthenticationProvider>(context, listen: false).logout();
            },
          ),
        ],
      ),
    );
  }
}
