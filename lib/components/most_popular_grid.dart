import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/components/card_product.dart';

import '../provider/product_provider.dart';

class MostPopularGrid extends StatelessWidget {
  const MostPopularGrid({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mostPopular = Provider.of<ProductsProvider>(context).mostPopular;
    return SizedBox(
      height: 300.0,
      child: GridView.builder(
        physics: const ScrollPhysics(parent: NeverScrollableScrollPhysics()),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisExtent: 300.0,
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
        ),
        itemCount: 2,
        itemBuilder: (ctx, index) => ChangeNotifierProvider.value(
          value: mostPopular[index],
          child: const CardProduct(),
        ),
      ),
    );
  }
}
