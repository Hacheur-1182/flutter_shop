import 'package:flutter/cupertino.dart';
import 'package:shop/helpers/color_app.dart';

class CardFooter extends StatelessWidget {
  const CardFooter({
    Key? key,
    required this.price,
    required this.name,
  }) : super(key: key);
  final String? name;
  final String? price;

  @override
  Widget build(BuildContext context) {
    TextStyle _priceStyle = TextStyle(
      color: kTextColor,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    );
    TextStyle _moneyStyle = TextStyle(
      color: kSelectedColor,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    );
    return Center(
      child: Column(
        children: [
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: [
                TextSpan(
                  text: "$name\n",
                  style: TextStyle(
                    color: kTextColor,
                  ),
                ),
                TextSpan(
                  text: "\$ ",
                  style: _moneyStyle,
                ),
                TextSpan(
                  text: price,
                  style: _priceStyle,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
