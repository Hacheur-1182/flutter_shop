import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/components/card_footer.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/helpers/url_api.dart';
import 'package:shop/routes/app_route.dart';

import '../models/product_model.dart';

class CardProduct extends StatelessWidget {
  const CardProduct({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final products = Provider.of<Product>(context);
    // print(products.isFavorite);
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(
              context,
              AppRoutes.detailPage,
              arguments: products.id,
            );
          },
          child: Hero(
            tag: products.id,
            child: Container(
              margin: const EdgeInsets.only(
                right: 10.0,
              ),
              height: 250.0,
              width: 175,
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(uriApi + products.image),
                ),
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Material(
                  borderOnForeground: false,
                  color: Colors.transparent,
                  child: IconButton(
                    onPressed: () {
                      products.isFavorite
                          ? products.deleteFavorite(products.id)
                          : products.setFav(products.id);
                    },
                    icon: Icon(
                      products.isFavorite
                          ? Icons.favorite
                          : Icons.favorite_border_outlined,
                      color: kButtonColor,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 10),
        CardFooter(
          name: products.name,
          price: "${products.price}",
        )
      ],
    );
  }
}
