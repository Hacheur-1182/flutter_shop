import 'package:flutter/material.dart';

import '../helpers/color_app.dart';

class FormInput extends StatelessWidget {
  const FormInput({
    Key? key,
    required this.labelName,
    this.saveEntry,
    this.validatorEntry,
    required this.obscure,
    this.suffixIcon,
    this.controller, this.enableField,
  }) : super(key: key);
  final String labelName;
  final bool obscure;
  final bool? enableField;
  final Widget? suffixIcon;
  final TextEditingController? controller;
  final void Function(String? value)? saveEntry;
  final String? Function(String? value)? validatorEntry;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enableField,
      controller: controller,
      obscureText: obscure,
      obscuringCharacter: "*",
      onSaved: saveEntry,
      validator: validatorEntry,
      cursorColor: kButtonColor,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        label: Padding(
          padding: const EdgeInsets.only(
            left: 18.0,
            right: 18.0,
          ),
          child: Text(
            labelName,
            style: TextStyle(
              color: kButtonColor,
            ),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: kSelectedColor,
          ),
          borderRadius: BorderRadius.circular(30.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: kSelectedColor,
          ),
          borderRadius: BorderRadius.circular(30.0),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.redAccent,
          ),
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }
}
