import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';

class CardCategory extends StatelessWidget {
  const CardCategory({Key? key, required this.category}) : super(key: key);
  final String category;
  // bool onTapped = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Logger().d("tapped");
      },
      child: Container(
        height: 40,
        width: 60,
        decoration: BoxDecoration(
          color: CupertinoColors.white,
          border: Border.all(
            color: const Color(0xffe0e0e0),
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            category,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontFamily: "fonts/Roboto-Regular.ttf",
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );
  }
}
