import 'package:flutter/material.dart';
import 'package:shop/routes/app_route.dart';

import '../helpers/color_app.dart';
import '../helpers/url_api.dart';

class CardFavorite extends StatelessWidget {
  const CardFavorite({
    Key? key,
    required this.name,
    required this.image,
    required this.price,
    required this.favoriteId,
    // required this.productId,
    // required this.color,
    // required this.size,
  }) : super(key: key);
  final String favoriteId;
  // final String productId;
  final String name;
  final String image;
  final double price;
  // final List<dynamic> color;
  // final List<dynamic> size;
  @override
  Widget build(BuildContext context) {
    TextStyle _priceStyle = TextStyle(
      color: kTextColor,
      fontWeight: FontWeight.bold,
      fontSize: 20.0,
      fontFamily: "Roboto",
    );
    TextStyle _moneyStyle = TextStyle(
      color: kSelectedColor,
      fontWeight: FontWeight.bold,
      fontSize: 20.0,
      fontFamily: "Roboto",
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(
              context,
              AppRoutes.detailPage,
              arguments: favoriteId,
            );
          },
          child: Container(
            margin: const EdgeInsets.only(right: 10.0),
            height: 200.0,
            width: 175,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(uriApi + image),
              ),
            ),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  name,
                  style: TextStyle(
                    color: kTextColor,
                    fontSize: 18.0,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 20.0,
                ),
                child: Text(
                  "Size: S",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 15.0,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "\$ ",
                      style: _moneyStyle,
                    ),
                    TextSpan(
                      text: "$price",
                      style: _priceStyle,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
