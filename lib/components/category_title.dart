import 'package:flutter/cupertino.dart';

class CategoryName extends StatelessWidget {
  const CategoryName({Key? key, this.name}) : super(key: key);
  final String? name;
  @override
  Widget build(BuildContext context) {
    return Text(name!);
  }
}
