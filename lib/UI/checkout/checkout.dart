import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/UI/cart/cart_button.dart';

import 'package:shop/helpers/color_app.dart';
import 'package:shop/helpers/url_api.dart';
import 'package:shop/provider/checkout_provider.dart';

class Checkout extends StatelessWidget {
  const Checkout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle _priceStyle = TextStyle(
      color: kTextColor,
      fontWeight: FontWeight.bold,
      fontSize: 15.0,
      fontFamily: "Roboto",
    );
    TextStyle _moneyStyle = TextStyle(
      color: kSelectedColor,
      fontWeight: FontWeight.bold,
      fontSize: 15.0,
      fontFamily: "Roboto",
    );

    final checkout = Provider.of<CheckoutProvider>(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color(0xffECEDEB),
        appBar: AppBar(
          backgroundColor: const Color(0xffECEDEB),
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.arrow_left,
              color: kButtonColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            "Checkout",
            style: TextStyle(
              color: kTextColor,
              fontFamily: "Roboto",
              fontSize: 18.0,
            ),
          ),
          centerTitle: true,
        ),
        body: checkout.checkout.isEmpty
            ? Center(
                heightFactor: 25.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // const Icon(
                    //   Icons.remove_shopping_cart_outlined,
                    //   size: 65.0,
                    // ),
                    Text(
                      "No Checkout!!",
                      style: TextStyle(
                        color: kTextColor,
                        fontSize: 20.0,
                      ),
                    ),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.only(
                  top: 18.0,
                  left: 20.0,
                  right: 20.0,
                ),
                child: Column(
                  children: [
                    // Expanded(
                    //   child: Column(
                    //     crossAxisAlignment: CrossAxisAlignment.start,
                    //     children: [
                    //       Text(
                    //         "Delivery Address",
                    //         style: TextStyle(
                    //           color: kTextColor,
                    //           fontSize: 15.0,
                    //           fontWeight: FontWeight.bold,
                    //         ),
                    //       ),
                    //       ListTile(
                    //         leading: Container(
                    //           padding: const EdgeInsets.all(10.0),
                    //           // margin: const EdgeInsets.all(8.0),
                    //           height: 60,
                    //           width: 60,
                    //           decoration: BoxDecoration(
                    //             color: Colors.white,
                    //             borderRadius: BorderRadius.circular(15.0),
                    //           ),
                    //           child: SvgPicture.asset(
                    //             "assets/icons/location_marker.svg",
                    //           ),
                    //         ),
                    //         title: const Padding(
                    //           padding: EdgeInsets.only(bottom: 12.0),
                    //           child: Text("Location"),
                    //         ),
                    //         subtitle: const Text("Yaounde,Cameroon"),
                    //         trailing: Icon(
                    //           CupertinoIcons.chevron_forward,
                    //           color: kButtonColor,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),

                    Expanded(
                      child: Column(
                        children: [
                          ListTile(
                            title: Text(
                              "My Cart",
                              style: TextStyle(
                                color: kTextColor,
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            trailing: Icon(
                              CupertinoIcons.chevron_forward,
                              color: kButtonColor,
                            ),
                          ),
                          Consumer<CheckoutProvider>(
                            builder:
                                (BuildContext context, value, Widget? child) {
                              return Column(
                                children: [
                                  Container(
                                    height: 500,
                                    decoration: const BoxDecoration(
                                      color: Color(0xffECEDEB),
                                    ),
                                    child: ListView.builder(
                                      physics: value.checkout.length <= 2
                                          ? const NeverScrollableScrollPhysics()
                                          : const AlwaysScrollableScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      itemCount: value.checkout.length,
                                      itemBuilder: (BuildContext context,
                                          int indexCheckout) {
                                        return Column(
                                          children: [
                                            Container(
                                              height: 130,
                                              decoration: const BoxDecoration(
                                                color: Color(0xffECEDEB),
                                              ),
                                              child: ListView.builder(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemCount: value
                                                    .checkout[indexCheckout]
                                                    .products
                                                    .length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  final products = value
                                                      .checkout[indexCheckout]
                                                      .products[index];
                                                  return Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        margin: const EdgeInsets
                                                            .only(
                                                          right: 10.0,
                                                        ),
                                                        height: 120.0,
                                                        width: 95,
                                                        clipBehavior:
                                                            Clip.hardEdge,
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15.0),
                                                          image:
                                                              DecorationImage(
                                                            fit: BoxFit.cover,
                                                            image: NetworkImage(
                                                              uriApi +
                                                                  products
                                                                      .image,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                              top: 8.0,
                                                              bottom: 8.0,
                                                              right: 50.0,
                                                            ),
                                                            child: Text(
                                                              products.name,
                                                              softWrap: true,
                                                              style: TextStyle(
                                                                color:
                                                                    kTextColor,
                                                                fontSize: 12.0,
                                                                fontFamily:
                                                                    "Roboto",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ),
                                                            ),
                                                          ),
                                                          const Spacer(),
                                                          Text(
                                                              "x ${products.quantity}"),
                                                          // const SizedBox(
                                                          //   height: 40,
                                                          // ),
                                                          const Spacer(),
                                                          RichText(
                                                            text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                  text: "\$ ",
                                                                  style:
                                                                      _moneyStyle,
                                                                ),
                                                                TextSpan(
                                                                  text:
                                                                      "${products.price}",
                                                                  style:
                                                                      _priceStyle,
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          const Spacer(),
                                                        ],
                                                      ),
                                                    ],
                                                  );
                                                },
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                const Text(
                                                  "SubTotal",
                                                  style: TextStyle(
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                const Spacer(),
                                                RichText(
                                                  text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                        text: "\$ ",
                                                        style: TextStyle(
                                                          color: kSelectedColor,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 25.0,
                                                          fontFamily: "Roboto",
                                                        ),
                                                      ),
                                                      TextSpan(
                                                        text: value
                                                            .checkout[
                                                                indexCheckout]
                                                            .amount
                                                            .toStringAsFixed(1),
                                                        style: TextStyle(
                                                          color: kTextColor,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 25.0,
                                                          fontFamily: "Roboto",
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              height: 40.0,
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      const Text(
                                        "Total",
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                      const Spacer(),
                                      RichText(
                                        text: TextSpan(
                                          children: [
                                            TextSpan(
                                              text: "\$ ",
                                              style: TextStyle(
                                                color: kSelectedColor,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 25.0,
                                                fontFamily: "Roboto",
                                              ),
                                            ),
                                            TextSpan(
                                              text: value.total
                                                  .toStringAsFixed(1),
                                              style: TextStyle(
                                                color: kTextColor,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 25.0,
                                                fontFamily: "Roboto",
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              );
                            },
                          ),
                        ],
                      ),
                    ),

                    const CardButton(textButton: "Pay Now"),
                    const SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
