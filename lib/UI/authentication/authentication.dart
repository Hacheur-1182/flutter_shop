import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/models/http_exception.dart';

import '../../components/form_input.dart';
import '../../provider/authentication_provider.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({Key? key}) : super(key: key);

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

enum authenticationMode { signIn, signUp }

class _AuthenticationScreenState extends State<AuthenticationScreen>
    with SingleTickerProviderStateMixin {
  bool _passObscure = true;
  bool _passConfirmObscure = true;
  bool _loading = false;

  authenticationMode _mode = authenticationMode.signIn;

  final GlobalKey<FormState> _formKey = GlobalKey();

  late AnimationController _controller;
  late Animation<double> _opacity;
  late Animation<Offset> _slider;

  final _password = TextEditingController();

  final Map<String, String> _authData = {
    "email": "",
    "password": "",
  };

  Future<void> _onSubmit() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    _formKey.currentState!.save();
    setState(() {
      _loading = true;
    });
    try {
      if (_mode == authenticationMode.signIn) {
        await Provider.of<AuthenticationProvider>(context, listen: false).login(
          _authData["email"],
          _authData["password"],
        );
      } else {
        await Provider.of<AuthenticationProvider>(context, listen: false)
            .signup(
          _authData["email"],
          _authData["password"],
        );
      }
    } on HttpException catch (error) {
      _showFlutterToast(error.toString());
    } catch (error) {
      //display message if internet connection is not
    }

    setState(() {
      _loading = false;
    });
  }

  void _changeMode() {
    if (_mode == authenticationMode.signIn) {
      setState(() {
        _mode = authenticationMode.signUp;
      });
      _controller.forward();
    } else {
      _mode = authenticationMode.signIn;
      _controller.reverse();
    }
  }

  void _showFlutterToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      backgroundColor: CupertinoColors.systemRed,
      textColor: Colors.white,
      fontSize: 14.0,
      gravity: ToastGravity.BOTTOM,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _opacity = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
    );

    _slider = Tween<Offset>(
      begin: const Offset(0, -1.5),
      end: const Offset(0, 0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,
      ),
    );

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 48.0),
                  child: Image(
                    image: AssetImage("assets/splashscreen/splashscreen.png"),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      FormInput(
                        obscure: false,
                        labelName: 'Email',
                        validatorEntry: (value) {
                          if (value!.isEmpty || !value.contains("@")) {
                            return "invalid email";
                          }
                          // return "invalid email";
                        },
                        saveEntry: (value) {
                          _authData["email"] = value!;
                        },
                      ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      FormInput(
                        labelName: "Password",
                        obscure: _passObscure,
                        validatorEntry: (value) {
                          if (value!.isEmpty || value.length < 6) {
                            return "Password is too short";
                          }
                          // return "Password is too short";
                        },
                        saveEntry: (value) {
                          _authData["password"] = value!;
                        },
                        controller: _password,
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              _passObscure = !_passObscure;
                            });
                          },
                          icon: Icon(
                            _passObscure
                                ? CupertinoIcons.eye_slash_fill
                                : CupertinoIcons.eye,
                            color: kButtonColor,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: _mode == authenticationMode.signUp ? 30.0 : 0,
                      ),
                      _mode == authenticationMode.signUp
                          ? SlideTransition(
                              position: _slider,
                              child: FadeTransition(
                                opacity: _opacity,
                                child: FormInput(
                                  enableField:
                                      _mode == authenticationMode.signUp,
                                  labelName: "Confirm Password",
                                  obscure: _passConfirmObscure,
                                  validatorEntry:
                                      _mode == authenticationMode.signUp
                                          ? (value) {
                                              if (value != _password.text) {
                                                return "Passwords do not match";
                                              }
                                              // return "Passwords do not match";
                                            }
                                          : null,
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _passConfirmObscure =
                                            !_passConfirmObscure;
                                      });
                                    },
                                    icon: Icon(
                                      _passConfirmObscure
                                          ? CupertinoIcons.eye_slash_fill
                                          : CupertinoIcons.eye,
                                      color: kButtonColor,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox(
                              height: 0.0,
                            ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        onPressed: () {
                          _onSubmit();
                        },
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                          backgroundColor: kSelectedColor,
                        ),
                        child: _loading
                            ? CircularProgressIndicator(
                                backgroundColor: kButtonColor,
                                color: kSelectedColor,
                              )
                            : Text(
                                _mode == authenticationMode.signIn
                                    ? "Sign In"
                                    : "Sign Up",
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            _mode == authenticationMode.signIn
                                ? "Don't have account"
                                : "Already have  account?",
                          ),
                          TextButton(
                            onPressed: () {
                              setState(() {
                                _changeMode();
                              });
                            },
                            child: Text(
                              _mode == authenticationMode.signIn
                                  ? "Sign Up"
                                  : "Sign In",
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
