import 'package:flutter/material.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/models/product_model.dart';

class SearchProduct extends SearchDelegate<Product> {
  @override
  List<Widget>? buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
        onPressed: () {
          query = "";
        },
        icon: const Icon(Icons.clear),
      ),
    ];
    // throw UnimplementedError();
  }

  @override
  Widget? buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      onPressed: () {
        // close(context, null);
        Navigator.pop(context);
      },
      icon: const Icon(
        Icons.arrow_back,
      ),
    );
    // throw UnimplementedError();
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return Container(
      height: 200,
      width: 200,
      decoration: BoxDecoration(
        color: kButtonColor,
      ),
    );
    // throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Text("");
    // throw UnimplementedError();
  }
}
