import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/provider/cart_provider.dart';
import 'package:shop/provider/product_provider.dart';

import '../../helpers/color_app.dart';
import '../../helpers/url_api.dart';

class DetailProduct extends StatefulWidget {
  const DetailProduct({Key? key}) : super(key: key);

  @override
  State<DetailProduct> createState() => _DetailProductState();
}

class _DetailProductState extends State<DetailProduct> {
  @override
  Widget build(BuildContext context) {
    List<String> sizes = ["A", "B", "C", "D"];
    List<bool> select = [true, false, false, false];
    int selectedIndex = 0;
    TextStyle _priceStyle = TextStyle(
      color: kTextColor,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    );
    TextStyle _moneyStyle = TextStyle(
      color: kSelectedColor,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
    );

    final prodId = ModalRoute.of(context)?.settings.arguments as String;
    final loadedProduct =
        Provider.of<ProductsProvider>(context, listen: true).findById(prodId);

    List<String> _size = [];
    List<String> _color = [];
    if (loadedProduct.size.isNotEmpty) {
      _size = loadedProduct.size.first.split(",");
    }
    if (loadedProduct.color.isNotEmpty) {
      _color = loadedProduct.color.first.split(",");
    }

    final cart = Provider.of<CartProvider>(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: kButtonColor,
            ),
          ),
          actions: [
            IconButton(
              onPressed: () {
                setState(() {
                  loadedProduct.isFavorite
                      ? loadedProduct.deleteFavorite(loadedProduct.id)
                      : loadedProduct.setFav(loadedProduct.id);
                });
              },
              icon: Icon(
                loadedProduct.isFavorite
                    ? Icons.favorite
                    : Icons.favorite_border_outlined,
                color: kButtonColor,
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(
            top: 10.0,
            left: 18.0,
            right: 18.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10.0,
                    bottom: 15.0,
                  ),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      TextSpan(
                        text: "${loadedProduct.name}\n\n",
                        style: _priceStyle,
                      ),
                      TextSpan(
                        text: "\$ ",
                        style: _moneyStyle,
                      ),
                      TextSpan(
                        text: "${loadedProduct.price}",
                        style: _priceStyle,
                      )
                    ]),
                  ),
                ),
              ),
              Hero(
                tag: loadedProduct.id,
                child: Container(
                  height: 250,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    image: DecorationImage(
                      image: NetworkImage(uriApi + loadedProduct.image),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 28.0),
                child: Text(
                  "Select Size",
                  style: _priceStyle,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: _size.isNotEmpty
                    ? _size
                        .map((e) => InkWell(
                              onTap: () {
                                setState(() {
                                  selectedIndex = sizes.indexOf(e);
                                  for (int i = 0; i < sizes.length; i++) {
                                    if (selectedIndex == i) {
                                      select[i] = true;
                                    } else {
                                      select[i] = false;
                                    }
                                  }
                                });
                                // print(select[selectedIndex]);
                                // print(select[0]);
                                // print(select[1]);
                                // print(select[2]);
                                // print(select[3]);
                              },
                              child: Container(
                                height: 40,
                                width: 40,
                                padding: const EdgeInsets.only(top: 10),
                                margin: const EdgeInsets.only(right: 8.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color(0xffe0e0e0),
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Text(
                                  e,
                                  style: TextStyle(
                                    color: kTextColor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ))
                        .toList()
                    : [
                        const Text(
                          "Empty",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                child: Text(
                  "Select Color",
                  style: _priceStyle,
                ),
              ),
              Row(
                children: _color.isNotEmpty
                    ? _color
                        .map(
                          (e) => Container(
                            height: 30,
                            width: 30,
                            padding: const EdgeInsets.only(top: 10),
                            margin: const EdgeInsets.only(right: 8.0),
                            decoration: BoxDecoration(
                              color: Color(int.parse("0xff$e")),
                              // color: Colors.white,
                              // border: Border.all(
                              //   color: kSelectedColor,
                              // ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        )
                        .toList()
                    : [
                        const Text(
                          "Empty",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ],
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 18.0),
                child: Row(
                  children: [
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "\$ ",
                            style: _moneyStyle,
                          ),
                          TextSpan(
                            text: "${loadedProduct.price}",
                            style: _priceStyle,
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: TextButton(
                        onPressed: () {
                          cart.addToCart(
                              loadedProduct.id,
                              loadedProduct.price,
                              loadedProduct.name,
                              loadedProduct.size,
                              loadedProduct.image);
                        },
                        style: TextButton.styleFrom(
                          backgroundColor: kButtonColor,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 35,
                            vertical: 15,
                          ),
                          // side: BorderSide,
                        ),
                        child: const Text(
                          "Add to Cart",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
