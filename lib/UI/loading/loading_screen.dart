import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../helpers/color_app.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          leading: Shimmer.fromColors(
            highlightColor: Colors.grey[100]!,
            baseColor: Colors.grey[300]!,
            child: Container(
              color: Colors.grey[300],
              margin: const EdgeInsets.only(left: 20, top: 15, bottom: 10),
            ),
          ),
          actions: [
            Shimmer.fromColors(
              highlightColor: Colors.grey[100]!,
              baseColor: Colors.grey[300]!,
              child: Container(
                height: 10.0,
                width: 35,
                decoration: BoxDecoration(color: Colors.grey[300]),
                margin: const EdgeInsets.only(left: 20, top: 15, bottom: 10),
              ),
            ),
            Shimmer.fromColors(
              highlightColor: Colors.grey[100]!,
              baseColor: Colors.grey[300]!,
              child: Container(
                height: 0.0,
                width: 35,
                decoration: BoxDecoration(color: Colors.grey[300]),
                margin: const EdgeInsets.only(left: 10,right: 10, top: 15, bottom: 10),
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(
            top: 8.0,
            left: kDefaultPadding,
            right: kDefaultPadding,
          ),
          child: SingleChildScrollView(
            physics: const ScrollPhysics(parent: NeverScrollableScrollPhysics()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Shimmer.fromColors(
                  highlightColor: Colors.grey[100]!,
                  baseColor: Colors.grey[300]!,
                  child: Container(
                    height: 40,
                    width: 200,
                    margin: const EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 35.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //Menu
                    SizedBox(
                      height: 40,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 4,
                        itemBuilder: (BuildContext context, int index) => Row(
                        children: [
                          Shimmer.fromColors(
                            highlightColor: Colors.grey[100]!,
                            baseColor: Colors.grey[300]!,
                            child: Container(
                              height: 40,
                              width: 80,
                              margin: const EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(
                                color: Colors.grey[300],
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        ],
                      ),
                      ),
                    ),
                    //Products
                    SizedBox(
                      height: 250,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: 2,
                        itemBuilder: (BuildContext context, int index) => Shimmer.fromColors(
                          highlightColor: Colors.grey[100]!,
                          baseColor: Colors.grey[300]!,
                          child: Container(
                            height: 250.0,
                            width: 175.0,
                            margin: const EdgeInsets.only(top: 10, right: 10,),
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    //Popular products
                    const SizedBox(height: 50,),
                    Row(children: [
                      Shimmer.fromColors(
                        highlightColor: Colors.grey[100]!,
                        baseColor: Colors.grey[300]!,
                        child: Container(
                          height: 20,
                          width: 130,
                          margin: const EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                      const Spacer(),
                      Shimmer.fromColors(
                        highlightColor: Colors.grey[100]!,
                        baseColor: Colors.grey[300]!,
                        child: Container(
                          height: 20,
                          width: 65,
                          margin: const EdgeInsets.only(right: 10),
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                    ],),
                    SizedBox(
                      height: 270,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: 2,
                        itemBuilder: (BuildContext context, int index) => Shimmer.fromColors(
                          highlightColor: Colors.grey[100]!,
                          baseColor: Colors.grey[300]!,
                          child: Container(
                            height: 250.0,
                            width: 175.0,
                            margin: const EdgeInsets.only(top: 20, right: 10,),
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// Center(
// heightFactor: 20.0,
// child: CircularProgressIndicator(
// backgroundColor: kButtonColor,
// color: Colors.white,
// ),
// )
