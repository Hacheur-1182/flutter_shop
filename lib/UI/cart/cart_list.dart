import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/provider/cart_provider.dart';

import '../../helpers/color_app.dart';
import 'cart_quantity.dart';

class CartList extends StatelessWidget {
  const CartList({
    Key? key,
    required TextStyle moneyStyle,
    required TextStyle priceStyle,
    required this.id,
    required this.name,
    required this.price,
    required this.image,
    required this.size,
    required this.quantity,
    this.addItem,
    this.removeItem,
  })  : _moneyStyle = moneyStyle,
        _priceStyle = priceStyle,
        super(key: key);

  final TextStyle _moneyStyle;
  final TextStyle _priceStyle;
  final String id;
  final String name;
  final String price;
  final String image;
  final List<dynamic> size;
  final int quantity;
  final void Function()? addItem;
  final void Function()? removeItem;

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context, listen: false);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(right: 10.0),
          height: 200.0,
          width: 175,
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(
                image,
              ),
            ),
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  "Casual",
                  style: TextStyle(
                    color: kTextColor,
                    fontSize: 18.0,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                "Jeans & Shoes",
                style: TextStyle(
                  color: kTextColor,
                  fontSize: 18.0,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                ),
                child: Text(
                  "Size: ${size.toString().replaceAll(RegExp(r'.$'), "").substring(1)}",
                  style: const TextStyle(
                    color: Colors.grey,
                    fontSize: 15.0,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "\$ ",
                      style: _moneyStyle,
                    ),
                    TextSpan(
                      text: price,
                      style: _priceStyle,
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Row(
                  children: [
                    CardQuantity(
                      icon: Icons.remove,
                      onTap: () {
                        if (quantity > 1) {
                          cart.updateQuantity(id, "decrement");
                        }
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 18.0,
                      ),
                      child: Text(
                        "$quantity",
                        style: TextStyle(
                          color: kTextColor,
                          fontWeight: FontWeight.bold,
                          fontFamily: "Roboto",
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                    CardQuantity(
                      icon: Icons.add,
                      onTap: () {
                        cart.updateQuantity(id, "increment");
                      },
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () {
                        cart.removeToCart(id);
                      },
                      icon: const Icon(
                        Icons.close,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
