import 'package:flutter/material.dart';
import 'package:shop/helpers/color_app.dart';

class CardQuantity extends StatelessWidget {
  const CardQuantity({Key? key, required this.icon, this.onTap})
      : super(key: key);
  final IconData? icon;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 25.0,
        width: 25.0,
        decoration: BoxDecoration(
          border: Border.all(color: kButtonColor),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Icon(
          icon,
          size: 20,
        ),
      ),
    );
  }
}
