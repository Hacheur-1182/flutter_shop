import 'package:flutter/cupertino.dart';

import '../../helpers/color_app.dart';

class CartPrice extends StatelessWidget {
  const CartPrice({
    Key? key,
    required TextStyle moneyStyle,
    required TextStyle priceStyle,
    required this.price,
    required this.title,
  })  : _moneyStyle = moneyStyle,
        _priceStyle = priceStyle,
        super(key: key);

  final TextStyle _moneyStyle;
  final TextStyle _priceStyle;
  final String price;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "$title :",
          style: TextStyle(
            color: kTextColor,
            fontSize: 16.0,
          ),
        ),
        const Spacer(),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "\$ ",
                style: _moneyStyle,
              ),
              TextSpan(
                text: price,
                style: _priceStyle,
              )
            ],
          ),
        ),
      ],
    );
  }
}