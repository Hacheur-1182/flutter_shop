import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/helpers/url_api.dart';
import 'package:shop/provider/cart_provider.dart';
import 'package:shop/provider/checkout_provider.dart';

import 'cart_button.dart';
import 'cart_list.dart';
import '../../routes/app_route.dart';
import 'cart_price.dart';

class Cart extends StatelessWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context);
    final checkout = Provider.of<CheckoutProvider>(context, listen: false);
    const double shipping = 10;
    final total = cart.total.first + 10;
    TextStyle _priceStyle = TextStyle(
      color: kTextColor,
      fontWeight: FontWeight.bold,
      fontSize: 20.0,
      fontFamily: "Roboto",
    );
    TextStyle _moneyStyle = TextStyle(
      color: kSelectedColor,
      fontWeight: FontWeight.bold,
      fontSize: 20.0,
      fontFamily: "Roboto",
    );
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: kButtonColor,
            ),
            onPressed: () {
              Navigator.pop(context);
              // Navigator.popAndPushNamed(context, AppRoutes.homePage);
            },
          ),
          title: Text(
            "My cart",
            style: TextStyle(
              color: kTextColor,
              fontFamily: "Roboto",
              fontSize: 18.0,
            ),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, AppRoutes.cartPage);
              },
              icon: SvgPicture.asset(
                "assets/icons/bag.svg",
                color: Colors.black,
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(
            top: 18.0,
            left: 20.0,
            right: 20.0,
          ),
          child: cart.itemCart.isNotEmpty
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        height: 400,
                        decoration: const BoxDecoration(
                          color: Colors.transparent,
                        ),
                        child: Consumer<CartProvider>(
                          builder:
                              (BuildContext context, value, Widget? child) {
                            return ListView.builder(
                              padding: const EdgeInsets.only(top: 10.0),
                              scrollDirection: Axis.vertical,
                              itemCount: value.itemCart.length,
                              itemBuilder: (BuildContext context, int index) {
                                return CartList(
                                  moneyStyle: _moneyStyle,
                                  priceStyle: _priceStyle,
                                  id: value.itemCart.values.toList()[index].id,
                                  name: value.itemCart.values
                                      .toList()[index]
                                      .name,
                                  price:
                                      '${value.itemCart.values.toList()[index].price}',
                                  image: uriApi +
                                      value.itemCart.values
                                          .toList()[index]
                                          .image,
                                  size: value.itemCart.values
                                      .toList()[index]
                                      .size,
                                  quantity: value.itemCart.values
                                      .toList()[index]
                                      .quantity,
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 200.0,
                      child: Column(
                        children: [
                          CartPrice(
                            moneyStyle: _moneyStyle,
                            priceStyle: _priceStyle,
                            price: cart.total.last.toStringAsFixed(2),
                            title: 'Sub Total',
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),
                          CartPrice(
                            moneyStyle: _moneyStyle,
                            priceStyle: _priceStyle,
                            title: 'Shipping',
                            price: "$shipping",
                          ),
                          const Divider(
                            color: Colors.grey,
                            height: 50,
                          ),
                          CartPrice(
                            moneyStyle: _moneyStyle,
                            priceStyle: _priceStyle,
                            title: 'Total',
                            price: total.toStringAsFixed(2),
                          ),
                          const SizedBox(
                            height: 12.0,
                          ),
                          // const Spacer(),
                          CardButton(
                            textButton: 'Checkout',
                            onPress: () {
                              checkout.addCheckout(
                                cart.itemCart.values.toList(),
                                cart.total.first,
                              );
                              Navigator.pushReplacementNamed(
                                context,
                                AppRoutes.checkoutPage,
                              );
                              cart.clear();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.remove_shopping_cart_outlined,
                        size: 65.0,
                      ),
                      Text(
                        "No items!!",
                        style: TextStyle(
                          color: kTextColor,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
