import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../helpers/color_app.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 48.0),
                  child: Image(
                    image: AssetImage("assets/splashscreen/splashscreen.png"),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Form(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        cursorColor: kButtonColor,
                        decoration: InputDecoration(
                          label: Padding(
                            padding: const EdgeInsets.only(
                              left: 18.0,
                              right: 18.0,
                            ),
                            child: Text(
                              "Email",
                              style: TextStyle(
                                color: kButtonColor,
                              ),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: kSelectedColor,
                            ),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: kSelectedColor,
                            ),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        cursorColor: kButtonColor,
                        obscureText: true,
                        obscuringCharacter: "*",
                        decoration: InputDecoration(
                          // contentPadding: EdgeInsets.symmetric(horizontal: 10.0,),
                          label: Padding(
                            padding: const EdgeInsets.only(
                              left: 18.0,
                              right: 18.0,
                            ),
                            child: Text(
                              "Password",
                              style: TextStyle(
                                color: kButtonColor,
                              ),
                            ),
                          ),
                          suffixIcon: Icon(
                            CupertinoIcons.eye_slash_fill,
                            color: kButtonColor,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: kSelectedColor,
                            ),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: kSelectedColor,
                            ),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(vertical: 15.0),
                          backgroundColor: kSelectedColor,
                        ),
                        child: const Text(
                          "Sign In",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
