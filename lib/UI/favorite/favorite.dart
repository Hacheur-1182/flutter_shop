import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/helpers/color_app.dart';

import '../../components/card_favorite.dart';
import '../../provider/product_provider.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    final favoriteLength =
        Provider.of<ProductsProvider>(context).favoriteItems.length;

    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Colors.white,
              pinned: true,
              title: Text(
                'You Love',
                style: TextStyle(
                  color: kTextColor,
                ),
              ),
              centerTitle: true,
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: kButtonColor,
                ),
              ),
            ),
            favoriteLength == 0
                ? SliverList(
                    delegate: SliverChildListDelegate([
                      Center(
                        heightFactor: 25.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // const Icon(
                            //   Icons.remove_shopping_cart_outlined,
                            //   size: 65.0,
                            // ),
                            Text(
                              "No items!!",
                              style: TextStyle(
                                color: kTextColor,
                                fontSize: 20.0,
                              ),
                            ),
                          ],
                        ),
                      )
                    ]),
                  )
                : SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Consumer<ProductsProvider>(
                                builder: (BuildContext context, value,
                                    Widget? child) {
                                  return CardFavorite(
                                    image: value.favoriteItems[index].image,
                                    name: value.favoriteItems[index].name,
                                    price: value.favoriteItems[index].price,
                                    favoriteId: value.favoriteItems[index].id,
                                  );
                                },
                              ),
                            ],
                          ),
                        );
                      },
                      childCount: favoriteLength,
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
