import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:shop/UI/search/search_product.dart';
import 'package:shop/helpers/color_app.dart';
import 'package:shop/provider/cart_provider.dart';
import 'package:shop/routes/app_route.dart';
import 'package:shop/side_bar/side_bar.dart';

import '../../components/card_product.dart';
import '../../components/category_title.dart';
import '../../provider/product_provider.dart';
import '../loading/loading_screen.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  bool _isLoading = false;
  @override
  void initState() {
    getData();

    super.initState();
  }

  Future<void> getData() async {
    try {
      InternetAddress.lookup("google.com").then((result) {
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          context.read<ProductsProvider>().getCategory();
          context.read<ProductsProvider>().getProducts().then((_) => {
                if (mounted)
                  {
                    setState(() {
                      _isLoading = false;
                    })
                  }
              });
        }
      }).catchError((onError) {
        Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
        Fluttertoast.showToast(
          msg: "Sorry, No internet connection",
          backgroundColor: Colors.redAccent,
          textColor: Colors.white,
        );
      });
    } on SocketException catch (_) {
      Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
      Fluttertoast.showToast(
        msg: "Sorry, No internet connection",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
      );
    }

    setState(() {
      _isLoading = true;
    });
  }

  void showFlutterToast(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        content: Row(
          children: [
            const Icon(Icons.wifi_off_outlined),
            Text(
              message,
              style: const TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<ScaffoldState>();
    final items = Provider.of<ProductsProvider>(context).items;
    final winter = Provider.of<ProductsProvider>(context).winter;
    final women = Provider.of<ProductsProvider>(context).women;
    final eyeWear = Provider.of<ProductsProvider>(context).eyeWear;
    final itemCartCount = Provider.of<CartProvider>(context).itemCount;
    final category =
        Provider.of<ProductsProvider>(context, listen: false).category;
    TabController _tabController = TabController(
      length: category.length,
      vsync: this,
    );
    return RefreshIndicator(
      onRefresh: () => getData(),
      backgroundColor: kButtonColor,
      color: Colors.white,
      child: _isLoading
          ? const LoadingScreen()
          : SafeArea(
              child: Scaffold(
                key: key,
                appBar: AppBar(
                  backgroundColor: Colors.white,
                  elevation: 0.0,
                  leading: IconButton(
                    icon: SvgPicture.asset(
                      "assets/icons/menu.svg",
                    ),
                    onPressed: () {
                      key.currentState!.openDrawer();
                    },
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        showSearch(
                          context: context,
                          delegate: SearchProduct(),
                        );
                      },
                      icon: SvgPicture.asset(
                        "assets/icons/search.svg",
                        color: Colors.black,
                      ),
                    ),
                    Stack(
                      children: [
                        Positioned(
                          top: 15,
                          right: 10,
                          child: Container(
                            height: 8.0,
                            width: 8.0,
                            decoration: BoxDecoration(
                              color: itemCartCount != 0
                                  ? kSelectedColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              AppRoutes.cartPage,
                            );
                          },
                          icon: SvgPicture.asset(
                            "assets/icons/bag.svg",
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                drawer: const SideBar(),
                body: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 8.0,
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Find your style",
                          style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: "fonts/Roboto-Regular.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 35.0,
                        ),
                        Column(
                          children: [
                            TabBar(
                              padding: const EdgeInsets.only(bottom: 20.0),
                              controller: _tabController,
                              labelColor: Colors.white,
                              unselectedLabelColor: kTextColor,
                              labelPadding: const EdgeInsets.all(10.0),
                              indicator: BoxDecoration(
                                color: Colors.black,
                                border: Border.all(
                                  color: const Color(0xffe0e0e0),
                                ),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              tabs: category
                                  .map((e) => CategoryName(
                                        name: e.name,
                                      ))
                                  .toList(),
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                color: Colors.transparent,
                              ),
                              width: double.maxFinite,
                              height: 300.0,
                              child: TabBarView(
                                controller: _tabController,
                                children: [
                                  ListView.builder(
                                    physics: const BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: items.length,
                                    itemBuilder: (context, index) {
                                      return ChangeNotifierProvider.value(
                                        value: items[index],
                                        child: const CardProduct(),
                                      );
                                    },
                                  ),
                                  ListView.builder(
                                    physics: const BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: winter.length,
                                    itemBuilder: (context, index) {
                                      return ChangeNotifierProvider.value(
                                        value: winter[index],
                                        child: const CardProduct(),
                                      );
                                    },
                                  ),
                                  ListView.builder(
                                    physics: const BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: women.length,
                                    itemBuilder: (context, index) {
                                      return ChangeNotifierProvider.value(
                                        value: women[index],
                                        child: const CardProduct(),
                                      );
                                    },
                                  ),
                                  ListView.builder(
                                    physics: const BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: eyeWear.length,
                                    itemBuilder: (context, index) {
                                      return ChangeNotifierProvider.value(
                                        value: eyeWear[index],
                                        child: const CardProduct(),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 25.0,
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                const Text(
                                  "Most Popular",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0,
                                  ),
                                ),
                                const Spacer(),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(
                                      context,
                                      AppRoutes.mostPopularPage,
                                    );
                                  },
                                  child: Text(
                                    "See all",
                                    style: TextStyle(
                                      color: kSelectedColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            // const MostPopularGrid(),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
