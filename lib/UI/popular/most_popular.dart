import 'package:flutter/material.dart';
import 'package:shop/components/most_popular_grid.dart';

import '../../helpers/color_app.dart';
import '../../components/most_popular_grid.dart';

class MostPopular extends StatelessWidget {
  const MostPopular({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text("MostPopular", style: TextStyle(color: kTextColor),),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back,
              color: kButtonColor,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: const [
              MostPopularGrid(),
            ],
          ),
        ),
      ),
    );
  }
}
