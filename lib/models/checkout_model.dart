import 'package:shop/provider/cart_provider.dart';

class CheckoutModel {
  final String id;
  final double amount;
  final List<CartItem> products;

  CheckoutModel({
    required this.id,
    required this.amount,
    required this.products,
  });
}
