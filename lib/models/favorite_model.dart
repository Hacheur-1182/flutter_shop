class FavoriteModel {
  final String productId;
  final String favoriteId;
  final String name;
  final double price;
  final List<dynamic> size;
  final List<dynamic> color;
  final String image;
  final String category;

  FavoriteModel( {
    required this.productId,
    required this.favoriteId,
    required this.name,
    required this.price,
    required this.size,
    required this.color,
    required this.image,
    required this.category,
  });
}
