

import 'package:flutter/cupertino.dart';

import '../helpers/url_api.dart';
import 'package:http/http.dart' as http;

import '../services/get_user.dart';

class Product with ChangeNotifier {
  final String id;
  final String name;
  final double price;
  final List<dynamic> size;
  final List<dynamic> color;
  final String image;
  final Map<String, dynamic> category;
  final bool popular;
  bool isFavorite;

  Product(
      {required this.id,
      required this.name,
      required this.price,
      required this.size,
      required this.color,
      required this.image,
      required this.category,
      required this.popular,
      this.isFavorite = false});

  void _toggleFavorite(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  Future<void> setFav(String prodId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;
    notifyListeners();

    final getUserIn = await getUser();

    var url = Uri.parse(uriApi + "/favorite");

    try {
      final response = await http.post(
        url,
        headers: {
          "ContentType": "application/json",
          "x-access-token": "${getUserIn.first}",
        },
        body: {
          "user_id": "${getUserIn.last}",
          "product_id": prodId,
        },
      );

      // final favStat = jsonDecode(response.body);
      //
      // print(favStat);
      // print(response.statusCode);

      if (response.statusCode >= 400) {
        _toggleFavorite(oldStatus);
      }
    } catch (error) {
      _toggleFavorite(oldStatus);
    }
  }
  Future<void> deleteFavorite(String prodId) async {
    final oldStatus = isFavorite;
    isFavorite = !isFavorite;

    notifyListeners();

    final getUserIn = await getUser();
    final url = Uri.parse(uriApi + "/favorite/product_id/$prodId");

    try {
      final response = await http.delete(url, headers: {
        "ContentType": "application/json",
        "x-access-token": "${getUserIn.first}",
      });

      // final deleteFavorite =
      // await json.decode(response.body) as Map<String, dynamic>;
      //
      // print(deleteFavorite);

      if(response.statusCode >= 400){
        _toggleFavorite(oldStatus);
      }
    } catch (error) {
      _toggleFavorite(oldStatus);
    }
  }
}
