import 'package:flutter/material.dart';
import 'package:shop/routes/app_route.dart';

class HandlingConnectionState extends StatelessWidget {
  const HandlingConnectionState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.wifi_off_rounded,
                    size: 45.0,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("No internet connection"),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, AppRoutes.homePage, (route) => false);
                    },
                    child: const Text("Retry"),
                    style: TextButton.styleFrom(minimumSize: Size.zero),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
