import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CartItem {
  final String id;
  final String name;
  final String image;
  final List<dynamic> size;
  // final String color;
  final int quantity;
  final double price;
  CartItem({
    required this.id,
    required this.name,
    required this.size,
    required this.image,
    // required this.color,
    required this.quantity,
    required this.price,
  });
}

class CartProvider with ChangeNotifier {
  Map<String, CartItem> _cartItem = {};

  Map<String, CartItem> get itemCart {
    return {..._cartItem};
  }

  int get itemCount {
    return _cartItem.length;
  }

  Set<double> get total {
    double total = 0.0;
    double subTotal = 0;
    _cartItem.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
      subTotal += cartItem.price * cartItem.quantity;
    });
    return {total, subTotal};
  }

  void updateQuantity(String prodId, String action) {
    switch (action) {
      case "increment":
        if (_cartItem.containsKey(prodId)) {
          _cartItem.update(
            prodId,
            (existingCartItem) => CartItem(
              id: existingCartItem.id,
              name: existingCartItem.name,
              size: existingCartItem.size,
              image: existingCartItem.image,
              quantity: existingCartItem.quantity + 1,
              price: existingCartItem.price,
            ),
          );
        }
        break;
      case 'decrement':
        if (_cartItem.containsKey(prodId)) {
          _cartItem.update(
            prodId,
            (existingCartItem) => CartItem(
              id: existingCartItem.id,
              name: existingCartItem.name,
              size: existingCartItem.size,
              image: existingCartItem.image,
              quantity: existingCartItem.quantity - 1,
              price: existingCartItem.price,
            ),
          );
        }
        break;
      default:
    }
    notifyListeners();
  }

  void addToCart(
    String prodId,
    double price,
    String name,
    List<dynamic> size,
    String image,
    // String color,
  ) {
    if (_cartItem.containsKey(prodId)) {
      //change quantity
      _cartItem.update(
        prodId,
        (existingCartItem) => CartItem(
          id: existingCartItem.id,
          name: existingCartItem.name,
          image: existingCartItem.image,
          size: existingCartItem.size,
          // color: existingCartItem.color,
          quantity: existingCartItem.quantity + 1,
          price: existingCartItem.price,
        ),
      );
    } else {
      //add in to cart
      _cartItem.putIfAbsent(
        prodId,
        () => CartItem(
          id: prodId,
          name: name,
          image: image,
          size: size,
          // color: color,
          quantity: 1,
          price: price,
        ),
      );
    }
    notifyListeners();
  }

  void removeToCart(String prodId) {
    _cartItem.remove(prodId);
    notifyListeners();
  }

  void clear() {
    _cartItem = {};
    notifyListeners();
  }
}
