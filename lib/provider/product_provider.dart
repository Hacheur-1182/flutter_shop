import 'dart:collection';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shop/models/favorite_model.dart';
import 'package:shop/models/http_exception.dart';
import 'package:shop/services/get_user.dart';
import '../helpers/url_api.dart';
import '../models/category_model.dart';
import '../models/product_model.dart';

class ProductsProvider with ChangeNotifier {
  List<Product> _items = [];
  List<CategoryModel> _category = [];
  List<FavoriteModel> _favorite = [];

  List<Product> _winter = [];
  List<Product> _women = [];
  List<Product> _eyeWear = [];
  List<Product> _mostPopular = [];

  UnmodifiableListView<Product> get items {
    return UnmodifiableListView(_items);
  }

  UnmodifiableListView<CategoryModel> get category {
    return UnmodifiableListView(_category);
  }

  UnmodifiableListView<Product> get winter {
    return UnmodifiableListView(_winter);
  }

  UnmodifiableListView<Product> get women {
    return UnmodifiableListView(_women);
  }

  UnmodifiableListView<Product> get eyeWear {
    return UnmodifiableListView(_eyeWear);
  }

  UnmodifiableListView<Product> get mostPopular {
    return UnmodifiableListView(_mostPopular);
  }

  UnmodifiableListView<FavoriteModel> get favorite {
    return UnmodifiableListView(_favorite);
  }

  void addProduct(Product product) {
    _items.add(product);
    notifyListeners();
  }

  Product findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }

  List<Product> get favoriteItems {
    return _items.where((element) => element.isFavorite).toList();
  }

  Future<void> getCategory() async {
    final url = Uri.parse(uriApi + "/category");
    final getTokenIn = await getUser();

    try {
      final response = await http.get(url, headers: {
        "ContentType": "application/json",
        "x-access-token": "${getTokenIn.first}",
      });

      final category = await json.decode(response.body) as Map<String, dynamic>;

      if (category["message"] == null) {
        return;
      }

      if (category["error"] != null) {
        throw HttpException("Something went wrong");
      }

      List<CategoryModel> categoryExtracted = [];

      for (var element in (category["message"] as List)) {
        categoryExtracted
            .add(CategoryModel(id: element["_id"], name: element["name"]));
      }
      _category = categoryExtracted;

      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> getProducts() async {
    final url = Uri.parse(uriApi + "/product");
    final getUserIn = await getUser();

    try {
      final response = await http.get(
        url,
        headers: {
          "ContentType": "application/json",
          "x-access-token": "${getUserIn.first}",
        },
      );

      final product = await json.decode(response.body) as Map<String, dynamic>;

      if (product["message"] == null) {
        return;
      }

      List<Product> extractedProduct = [];
      List<Product> extractedWinterProduct = [];
      List<Product> extractedWomenProduct = [];
      List<Product> extractedEyeWearProduct = [];
      List<Product> extractedMostPopularProduct = [];

      for (var element in (product["message"] as List)) {
        final favStatus = await getIsFavorite(element["_id"]);

        extractedProduct.add(Product(
          id: element['_id'],
          size: element['clotheSize'],
          image: element['image'],
          color: element['clotheColor'],
          price: element['price'].toDouble(),
          name: element['name'],
          category: element['category'],
          popular: element['popular'] ?? false,
          isFavorite: favStatus,
        ));
      }
      _items = extractedProduct;

      for (var element in extractedProduct) {
        if (element.category["name"] == "Winter") {
          extractedWinterProduct.add(element);
        } else if (element.category["name"] == "Women") {
          extractedWomenProduct.add(element);
        } else if (element.category["name"] == "Eyewear") {
          extractedEyeWearProduct.add(element);
        }
        if (element.popular) {
          extractedMostPopularProduct.add(element);
        }
      }
      _winter = extractedWinterProduct;
      _women = extractedWomenProduct;
      _eyeWear = extractedEyeWearProduct;
      _mostPopular = extractedMostPopularProduct;

      if (product["error"] != null) {
        throw HttpException("Something went wrong");
      }
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> getFavorite() async {
    final getUserIn = await getUser();

    final url = Uri.parse(uriApi + "/favorite/user_id/${getUserIn.last}");

    try {
      final response = await http.get(
        url,
        headers: {
          "ContentType": "application/json",
          "x-access-token": "${getUserIn.first}",
        },
      );

      final favorite = await json.decode(response.body) as Map<String, dynamic>;

      List<FavoriteModel> extractedFavorite = [];

      for (var element in (favorite["message"] as List)) {
        print(element);
        extractedFavorite.add(FavoriteModel(
          favoriteId: element["favorite_id"],
          productId: element["product_id"],
          name: element["name"],
          price: element["price"].toDouble(),
          size: element["clotheSize"],
          color: element["clotheColor"],
          image: element["image"],
          category: element["category"],
        ));
      }

      _favorite = extractedFavorite;

      notifyListeners();
    } catch (_) {}
  }

  Future<bool> getIsFavorite(String productId) async {
    final getUserIn = await getUser();

    bool _isFav = false;
    final url = Uri.parse(uriApi + "/favorite/product_id/$productId");

    try {
      final response = await http.get(url, headers: {
        "ContentType": "application/json",
        "x-access-token": "${getUserIn.first}",
      });

      final favorite = await json.decode(response.body) as Map<String, dynamic>;

      if (favorite["status"] == true) {
        // print("yeah");
        _isFav = true;
        return _isFav;
      }

      // notifyListeners();
    } catch (_) {}
    return _isFav;
  }
}
