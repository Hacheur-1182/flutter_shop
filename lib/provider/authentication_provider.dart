import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop/helpers/url_api.dart';
import 'package:http/http.dart' as http;
import 'package:shop/models/http_exception.dart';

class AuthenticationProvider with ChangeNotifier {
  String? _token;
  DateTime? _expireTokenTime;
  String? _userId;

  bool get isAuthenticated {
    return token != null;
  }

  String? get token {
    if (_expireTokenTime != null &&
        _expireTokenTime!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  Future<void> _authenticationRequest(
    String? email,
    String? password,
    String route,
  ) async {
    try {
      var url = Uri.parse(uriApi + route);
      final response = await http.post(
        url,
        headers: {
          "ContentType": "application/json",
          "Accept": "application/json",
        },
        body: {
          "email": email,
          "password": password,
        },
      ).timeout(
        const Duration(seconds: 30),
        onTimeout: () => throw HttpException("Missing internet connection"),
      );
      final responseMessage = jsonDecode(response.body);

      if (responseMessage["error"] != null) {
        throw HttpException(responseMessage["error"]);
      }

      _userId = responseMessage["_id"];
      _token = responseMessage["token"];
      _expireTokenTime = DateTime.now().add(
        Duration(
          seconds: responseMessage["expiresIn"],
        ),
      );
      // if (kDebugMode) {
      //   print(jsonDecode(response.body));
      //   // print(_expireTokenTime);
      // }
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final user = jsonEncode({
        '_id': _userId,
        'token': _token,
        'expiresIn': _expireTokenTime?.toIso8601String()
      });
      prefs.setString('user', user);
    } catch (error) {
      rethrow;
    }
  }

  void logout() async {
    _userId = null;
    _token = null;
    _expireTokenTime = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("user")) {
      return false;
    }
    final retrieveUser =
        jsonDecode(prefs.getString("user")!) as Map<String, dynamic>?;
    final expireIn = DateTime.parse("${retrieveUser!["expiresIn"]}");
    if (expireIn.isBefore(DateTime.now())) {
      return false;
    }
    _userId = retrieveUser['_id'] as String?;
    _token = retrieveUser['expiresIn'] as String?;
    _expireTokenTime = expireIn;
    notifyListeners();
    return true;
  }

  Future<void> signup(String? email, String? password) async {
    return _authenticationRequest(email, password, "/signup");
  }

  Future<void> login(String? email, String? password) async {
    return _authenticationRequest(email, password, "/login");
  }
}
