import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:shop/models/checkout_model.dart';
import 'package:shop/provider/cart_provider.dart';

class CheckoutProvider with ChangeNotifier {
  final List<CheckoutModel> _checkout = [];

  UnmodifiableListView<CheckoutModel> get checkout {
    return UnmodifiableListView(_checkout);
  }

  double get total {
    double total = 0.0;
    for (var item in _checkout) {
      total += item.amount;
    }
    return total;
  }

  void addCheckout(List<CartItem> cartProd, double amount) {
    _checkout.insert(
      0,
      CheckoutModel(
        products: cartProd,
        id: '1',
        amount: amount,
      ),
    );
    for(var item in _checkout){

        print(item.amount);

    }
    //print(_checkout.length);
    notifyListeners();
  }
}
