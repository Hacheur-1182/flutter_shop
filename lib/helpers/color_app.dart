import 'dart:ui';

Color kButtonColor = const Color(0xff000000);
Color kTextColor = const Color(0xff0E0E0E);
Color kSelectedColor = const Color(0xffFB975E);
Color kPrimaryColor = const Color(0xffF9F9F9);
const double kDefaultPadding = 20.0;

